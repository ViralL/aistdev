'use strict';

//priorityNav
function priorityNav(priorityContainer) {
  const priorityMenu = priorityContainer.querySelector('.priority');
  const overflowMenu = priorityContainer.querySelector('.overflow');
  const overflowTrigger = priorityContainer.querySelector('.js-overflow-trigger');
  const menuItems = priorityMenu.children;
  const delay = 300;
  let throttled = false;
  let breakout = [];
  let timeout = 0;

  // Helpers could be shipped as part of an Object Literal/Module
  const throttle = (callback, delay, scope) => {
    if (!throttled) {
      callback.call(scope);
      throttled = true;

      setTimeout(function() {
        throttled = false;
      }, delay);
    }
  };

  // Initialise
  const initialise = () => {
    checkNav();
    addListeners();
  }

  /* return the current width of the Navigation */
  const getPriorityWidth = () => {
    return priorityMenu.offsetWidth + 1;
  }

  /* return the break point of a menu item */
  const getItemBreakPoint = item => {
    return item.offsetLeft + item.offsetWidth;
  }

  /* test breakpoint against menu width */
  const itemBreaks = (breakPoint, menuWidth) => {
    return breakPoint > menuWidth;
  }

  /* test menuWidth against breakOut */
  const itemBreaksOut = (index, priorityWidth) => {
    if (breakout[index] < priorityWidth) {
      return true;
    }
  }

  /* move item to overflow menu */
  const addToOverflow = (item, itemBreakPoint) => {
    overflowMenu.insertBefore(item, overflowMenu.firstChild);
    breakout.unshift(itemBreakPoint);
  }

  /* remove from the overflwo menu */
  const removeFromOverflow = (breaksOut) => {
    for (let item of breaksOut) {
      breakout.shift();
      priorityMenu.appendChild(item);
    }
  }

  /* Set button visibility */
  const checkTriggerHidden = (value) => {
    if (value.toString() != overflowTrigger.getAttribute('aria-hidden')) {
      overflowTrigger.setAttribute('aria-hidden', value);
      checkNav();
    }
  }

  /* Check priority and overflow */
  const checkNav = () => {
    /* check priorityMenu */
    let priorityWidth = getPriorityWidth();

    /* Iterate over the priority menu */
    let priorityIndex = menuItems.length;

    while (priorityIndex--) {
      let item = menuItems[priorityIndex];
      let itemBreakPoint = getItemBreakPoint(item);

      if (itemBreaks(itemBreakPoint, priorityWidth)) {
        addToOverflow(item, itemBreakPoint);
      };
    };

    /* iterate the overflow */
    let overflowIndex = overflowMenu.children.length;
    let breaksOut = [];

    while (overflowIndex--) {
      if (itemBreaksOut(overflowIndex, priorityWidth)) {
        breaksOut.unshift(overflowMenu.children[overflowIndex]);
      }
    }

    removeFromOverflow(breaksOut);

    /* check the trigger visibility */
    checkTriggerHidden(breakout.length == 0);
  }

  /* Add Event listeners */
  const addListeners = () => {
    window.addEventListener('resize', () => {
      //throttle
      throttle(checkNav, delay);
      //debounce
      if (timeout) {
        clearTimeout(timeout);
      }

      timeout = setTimeout(() => {
        checkNav();
      }, delay);
    })

    overflowTrigger.addEventListener('click', () => {
      overflowMenu.setAttribute('aria-hidden', overflowMenu.getAttribute('aria-hidden') === 'true' ? 'false' : 'true');
    })
  }

  /* Initilaise the menu */
  initialise();
}

const priorityContainer1 = document.querySelector('.priority-menu--js1');
const priorityContainer2 = document.querySelector('.priority-menu--js2');
const priorityContainer3 = document.querySelector('.priority-menu--js3');

priorityNav(priorityContainer1);
priorityNav(priorityContainer2);
priorityNav(priorityContainer3);

//priorityNav

// ready
$(document).ready(function() {

    // search
    $('.search--js').click(function () {
        $(this).toggleClass('active').prev().slideToggle();
        return false;
    });
    $('.close--js').click(function () {
        $(this).closest('.search--body').hide();
        $(this).parents().find('body').removeClass('shadow');
        return false;
    });
    $('.show--js').click(function () {
        $(this).parent().hide().next().slideDown();
        return false;
    });
    $('.main-nav__toggle--js').click(function () {
        $(this).next().slideToggle();
        return false;
    });
    // search

    // mask phone {maskedinput}
    //$("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone


    $('.test__item').each( function() {
        $('input').on('change', function() {
            $(this).parent().parent().next().show();
        });
    });

    // select {select2}
    $('select').select2({
        minimumResultsForSearch: Infinity
    });
    // select

    // popup {magnific-popup}
    $('.popup').magnificPopup({
  		type: 'inline'
  	});
  	$(document).on('click', '.popup-modal-dismiss', function (e) {
  		e.preventDefault();
  		$.magnificPopup.close();
  	});
    // popup


    //.slider-line--js
    $('.slider-line--js').each(function(){
        var per = $(this).attr('data-per');
        $(this).css('width', per + '%');
        if ( per > 80 ) {
            $(this).css('background-color','#ffab6e');
        } else if ( per > 60 && per <= 80) {
            $(this).css('background-color','#ffd7bb');
        } else if ( per > 40 && per <= 60) {
            $(this).css('background-color','#76d8e4');
        } else {
            $(this).css('background-color','#b5e6e5');
        }
    });
    //.slider-line--js


    //tabs
    $('ul.tabs__caption').on('click', 'li:not(.active)', function() {
        $(this)
            .addClass('active').siblings().removeClass('active')
            .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
    });
    //tabs



});
// ready

$(window).scroll(function(){
  if ($(window).scrollTop() >= $('.page-header').height()) {
     $('.page-header__js').addClass('fixed');
  }
  else {
     $('.page-header__js').removeClass('fixed');
  }
});
