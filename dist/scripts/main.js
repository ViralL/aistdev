'use strict';

//priorityNav
function priorityNav(priorityContainer) {
  var priorityMenu = priorityContainer.querySelector('.priority');
  var overflowMenu = priorityContainer.querySelector('.overflow');
  var overflowTrigger = priorityContainer.querySelector('.js-overflow-trigger');
  var menuItems = priorityMenu.children;
  var delay = 300;
  var throttled = false;
  var breakout = [];
  var timeout = 0;

  // Helpers could be shipped as part of an Object Literal/Module
  var throttle = function throttle(callback, delay, scope) {
    if (!throttled) {
      callback.call(scope);
      throttled = true;

      setTimeout(function () {
        throttled = false;
      }, delay);
    }
  };

  // Initialise
  var initialise = function initialise() {
    checkNav();
    addListeners();
  };

  /* return the current width of the Navigation */
  var getPriorityWidth = function getPriorityWidth() {
    return priorityMenu.offsetWidth + 1;
  };

  /* return the break point of a menu item */
  var getItemBreakPoint = function getItemBreakPoint(item) {
    return item.offsetLeft + item.offsetWidth;
  };

  /* test breakpoint against menu width */
  var itemBreaks = function itemBreaks(breakPoint, menuWidth) {
    return breakPoint > menuWidth;
  };

  /* test menuWidth against breakOut */
  var itemBreaksOut = function itemBreaksOut(index, priorityWidth) {
    if (breakout[index] < priorityWidth) {
      return true;
    }
  };

  /* move item to overflow menu */
  var addToOverflow = function addToOverflow(item, itemBreakPoint) {
    overflowMenu.insertBefore(item, overflowMenu.firstChild);
    breakout.unshift(itemBreakPoint);
  };

  /* remove from the overflwo menu */
  var removeFromOverflow = function removeFromOverflow(breaksOut) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = breaksOut[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var item = _step.value;

        breakout.shift();
        priorityMenu.appendChild(item);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator['return']) {
          _iterator['return']();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  };

  /* Set button visibility */
  var checkTriggerHidden = function checkTriggerHidden(value) {
    if (value.toString() != overflowTrigger.getAttribute('aria-hidden')) {
      overflowTrigger.setAttribute('aria-hidden', value);
      checkNav();
    }
  };

  /* Check priority and overflow */
  var checkNav = function checkNav() {
    /* check priorityMenu */
    var priorityWidth = getPriorityWidth();

    /* Iterate over the priority menu */
    var priorityIndex = menuItems.length;

    while (priorityIndex--) {
      var item = menuItems[priorityIndex];
      var itemBreakPoint = getItemBreakPoint(item);

      if (itemBreaks(itemBreakPoint, priorityWidth)) {
        addToOverflow(item, itemBreakPoint);
      };
    };

    /* iterate the overflow */
    var overflowIndex = overflowMenu.children.length;
    var breaksOut = [];

    while (overflowIndex--) {
      if (itemBreaksOut(overflowIndex, priorityWidth)) {
        breaksOut.unshift(overflowMenu.children[overflowIndex]);
      }
    }

    removeFromOverflow(breaksOut);

    /* check the trigger visibility */
    checkTriggerHidden(breakout.length == 0);
  };

  /* Add Event listeners */
  var addListeners = function addListeners() {
    window.addEventListener('resize', function () {
      //throttle
      throttle(checkNav, delay);
      //debounce
      if (timeout) {
        clearTimeout(timeout);
      }

      timeout = setTimeout(function () {
        checkNav();
      }, delay);
    });

    overflowTrigger.addEventListener('click', function () {
      overflowMenu.setAttribute('aria-hidden', overflowMenu.getAttribute('aria-hidden') === 'true' ? 'false' : 'true');
    });
  };

  /* Initilaise the menu */
  initialise();
}

var priorityContainer1 = document.querySelector('.priority-menu--js1');
var priorityContainer2 = document.querySelector('.priority-menu--js2');
var priorityContainer3 = document.querySelector('.priority-menu--js3');

priorityNav(priorityContainer1);
priorityNav(priorityContainer2);
priorityNav(priorityContainer3);

//priorityNav

// ready
$(document).ready(function () {

  // search
  $('.search--js').click(function () {
    $(this).toggleClass('active').prev().slideToggle();
    return false;
  });
  $('.close--js').click(function () {
    $(this).closest('.search--body').hide();
    $(this).parents().find('body').removeClass('shadow');
    return false;
  });
  $('.show--js').click(function () {
    $(this).parent().hide().next().slideDown();
    return false;
  });
  $('.main-nav__toggle--js').click(function () {
    $(this).next().slideToggle();
    return false;
  });
  // search

  // mask phone {maskedinput}
  //$("[name=phone]").mask("+7 (999) 999-9999");
  // mask phone

  $('.test__item').each(function () {
    $('input').on('change', function () {
      $(this).parent().parent().next().show();
    });
  });

  // select {select2}
  $('select').select2({
    minimumResultsForSearch: Infinity
  });
  // select

  // popup {magnific-popup}
  $('.popup').magnificPopup({
    type: 'inline'
  });
  $(document).on('click', '.popup-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });
  // popup

  //.slider-line--js
  $('.slider-line--js').each(function () {
    var per = $(this).attr('data-per');
    $(this).css('width', per + '%');
    if (per > 80) {
      $(this).css('background-color', '#ffab6e');
    } else if (per > 60 && per <= 80) {
      $(this).css('background-color', '#ffd7bb');
    } else if (per > 40 && per <= 60) {
      $(this).css('background-color', '#76d8e4');
    } else {
      $(this).css('background-color', '#b5e6e5');
    }
  });
  //.slider-line--js

  //tabs
  $('ul.tabs__caption').on('click', 'li:not(.active)', function () {
    $(this).addClass('active').siblings().removeClass('active').closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
  });
  //tabs
});
// ready

$(window).scroll(function () {
  if ($(window).scrollTop() >= $('.page-header').height()) {
    $('.page-header__js').addClass('fixed');
  } else {
    $('.page-header__js').removeClass('fixed');
  }
});
//# sourceMappingURL=main.js.map
